
function agrega_a_body(obj_forma){
document.getElementById('body');
document.body.appendChild(obj_forma);
}

function crear_input_text(nombre,placeholder){
var obj_input=document.createElement('input');
obj_input.name=nombre;
obj_input.type="text";
obj_input.size=10;
obj_input.placeholder=placeholder;
return obj_input;
}

function creaContra(nombrecontra){
var obj_contra=document.createElement('input');
obj_contra.name=nombrecontra;
obj_contra.type="password";
obj_contra.size=15;
obj_contra.placeholder="contraseña";
return obj_contra;
}

function crear_select(name){
var obj_select=document.createElement('select');
obj_select.id=name;
return obj_select;
}

function agregar_padre_hijo(padre, hijo){
document.getElementById(padre);
padre.appendChild(hijo);
}

function crea_opcion(nombre,opc1){
var obj_opc=document.createElement('option');
obj_opc.value=nombre;
obj_opc.textContent=opc1;
return obj_opc;
}

function agregaOpc(select, opcion){
document.getElementById(select);
select.appendChild(opcion);
}

function crear_textarea(nombre,placeholder, columnas, renglones){
var texta=document.createElement('textarea');
texta.id=nombre;
texta.placeholder=placeholder;
texta.cols=columnas;
texta.rows=renglones;
texta.wrap="soft";

return texta;
}

function crear_progres(nombre, min,max, valor){
var progres=document.createElement('input');
progres.type='progress';
progres.id=nombre;
progres.min=min;
progres.max=max;
progres.value=valor;

return progres;
}

function crear_number(nombre, min, max){
var number=document.createElement('input');
number.type='number';
number.max=max;
number.min=min;

return number;
}

function crear_etiqH1(contenido){
var etih1=document.createElement('H1');
etih1.textContent=contenido;

return etih1;
}

function crear_etiqH2(contenido){
var etih2=document.createElement('H2');
etih2.textContent=contenido;

return etih2;
}

function crear_etiqH3(contenido){
var etih3=document.createElement('H3');
etih3.textContent=contenido;

return etih3;
}

function crear_etiqH4(contenido){
var etih4=document.createElement('H4');
etih4.textContent=contenido;

return etih4;
}

function crear_form(nombre){
var obj_form=document.createElement('form');
obj_form.id=nombre;
obj_form.role='form';
return obj_form;
}

function crear_clase1(clase){
var div1=document.createElement('div');
div1.setAttribute("class","col-md-1");

return div1;
}

function crear_clase4(clase){
var div4=document.createElement('div');
div4.setAttribute("class","col-md-4");
return div4;
}

function crear_clase6(clase){
var div6=document.createElement('div');
div6.setAttribute("class","col-md-6");
return div6;
}

function crear_clase8(clase){
var div8=document.createElement('div');
div8.setAttribute("class","col-md-8");
return div8;
}

function agregar_clase_forma(forma, clase){
document.getElementById(forma);
forma.appendChild(clase);
}

function agregar_clas_elem(clase, elemento){
document.getElementById(clase);
clase.appendChild(elemento);
}


